import colors from "vuetify/es5/util/colors";

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "Smart Domestic",
    title: "Smart Domestic",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    "@nuxtjs/vuetify",
    "@nuxtjs/dotenv",
    "@nuxtjs/axios",
    "cookie-universal-nuxt",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    [
      "nuxt-i18n",
      {
        locales: [
          {
            name: "Thai",
            code: "th",
            iso: "th-TH",
            file: "th-TH.js",
          },
          {
            name: "English",
            code: "en",
            iso: "en-US",
            file: "en-US.js",
          },
          {
            name: "Myanma",
            code: "MM",
            iso: "mm-MM",
            file: "mm-MM.js",
          },
          {
            name: "Cambodia",
            code: "KH",
            iso: "kh-KH",
            file: "kh-KH.js",
          },
        ],
        langDir: "lang/",
        defaultLocale: "th",
        detectBrowserLanguage: false,
      },
    ],
  ],

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    treeShake: true,
    font: {
      family: "Helvetica",
    },
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
        light: {
          "ci-orange": "#F54F1D",
          "ci-blue": "#1867C0",
          "ci-red": "#FF0000",
          "ci-grey": "#BBBDBF",
          "ci-grey-lt1": "#C4C4C4",
          "ci-grey-lt2": "#E0E0E0",
          "ci-grey-lt3": "#F8F8F8",
          "ci-grey-dk1": "#888888",
          "ci-grey-dk2": "#666666",
          "ci-grey-dk3": "#232323",
        },
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
  axios: {
    baseURL: `${process.env.SERVER_URL}`,
  },
  server: {
    port: `${process.env.PORT}`,
    host: "0.0.0.0",
  },
};
